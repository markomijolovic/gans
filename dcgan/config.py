import torch

torch.backends.cudnn.benchmark = True

DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
IMG_SIZE = 64
LEARNING_RATE = 2e-4
BATCH_SIZE = 128
NUM_EPOCHS = 10
LOAD_MODEL = False
SAVE_MODEL = True
TB_UPDATE_STEP = 10
PATH_TO_CELEBA = '/home/marko/Downloads/celeba_hq/train'
FIXED_NOISE = torch.randn(32, 100, 1, 1).to(DEVICE)
