import torch.nn as nn


class Discriminator(nn.Module):
    def __init__(self, d_features=64, img_channels=1):
        super().__init__()

        self.__model = nn.Sequential(
            nn.Conv2d(in_channels=img_channels, out_channels=d_features,
                      kernel_size=4, stride=2, padding=1),
            nn.LeakyReLU(0.2),

            nn.Conv2d(in_channels=d_features, out_channels=d_features*2,
                      kernel_size=4, stride=2, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2),

            nn.Conv2d(in_channels=d_features*2, out_channels=d_features*4,
                      kernel_size=4, stride=2, padding=1),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2),

            nn.Conv2d(in_channels=d_features*4, out_channels=d_features*8,
                      kernel_size=4, stride=2, padding=1),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2),

            nn.Conv2d(in_channels=d_features*8, out_channels=1,
                      kernel_size=4, stride=2, padding=0),
            nn.Sigmoid(),
        )

    def forward(self, x):
        return self.__model(x)


class Generator(nn.Module):
    def __init__(self, g_features=128, img_channels=1, latent_dims=100):
        super().__init__()
        self.__model = nn.Sequential(
            nn.ConvTranspose2d(in_channels=latent_dims, out_channels=g_features*8,
                               kernel_size=4, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(1024),
            nn.ReLU(),

            nn.ConvTranspose2d(in_channels=g_features*8, out_channels=g_features*4,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(512),
            nn.ReLU(),

            nn.ConvTranspose2d(in_channels=g_features*4, out_channels=g_features*2,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(),

            nn.ConvTranspose2d(in_channels=g_features*2, out_channels=g_features,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(),

            nn.ConvTranspose2d(in_channels=g_features, out_channels=img_channels,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.Tanh()
        )

    def forward(self, x):
        return self.__model(x)
