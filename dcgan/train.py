from pathlib import Path
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

import torchvision
import torchvision.datasets as datasets
import torchvision.transforms as transforms

import config
from model import Generator, Discriminator
from utils import *


if __name__ == '__main__':
    trans = transforms.Compose([
        transforms.Resize(config.IMG_SIZE),
        transforms.CenterCrop(config.IMG_SIZE),
        transforms.ToTensor(),
        transforms.Normalize([0.5], [0.5])])

    dataset = datasets.MNIST(
        root='datasets', transform=trans, download=True, train=True)
    loader = DataLoader(dataset, batch_size=config.BATCH_SIZE, shuffle=True)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    generator = Generator().to(device)
    discriminator = Discriminator().to(device)

    criterion = nn.BCELoss()

    optim_g = optim.Adam(generator.parameters(),
                         lr=config.LEARNING_RATE, betas=(0.5, 0.999))
    optim_d = optim.Adam(discriminator.parameters(),
                         lr=config.LEARNING_RATE, betas=(0.5, 0.999))

    writer = SummaryWriter('logs/dcgan')

    if config.LOAD_MODEL:
        load(generator, optim_g, config.LEARNING_RATE, 'generator.pth')
        load(discriminator, optim_d, config.LEARNING_RATE, 'discriminator.pth')

    tb_update_idx = 0
    for epoch_idx in range(config.NUM_EPOCHS):
        for batch_idx, (real, _) in enumerate(tqdm(loader)):
            real = real.to(device)
            noise = torch.randn((config.BATCH_SIZE, 100, 1, 1)).to(device)

            fake = generator(noise)

            # train discriminator
            d_real = discriminator(real).reshape(-1)
            loss_d_real = criterion(d_real, torch.ones_like(d_real))

            d_fake = discriminator(fake).reshape(-1)
            loss_d_fake = criterion(d_fake, torch.zeros_like(d_fake))

            loss_d = (loss_d_real+loss_d_fake)/2

            discriminator.zero_grad()
            loss_d.backward(retain_graph=True)
            optim_d.step()

            # train generator
            y = discriminator(fake).reshape(-1)
            loss_g = criterion(y, torch.ones_like(y))

            generator.zero_grad()
            loss_g.backward()
            optim_g.step()

            if batch_idx % config.TB_UPDATE_STEP == 0:
                with torch.no_grad():
                    fake = generator(config.FIXED_NOISE)
                    img_grid_real = torchvision.utils.make_grid(
                        real[:32], normalize=True
                    )
                    img_grid_fake = torchvision.utils.make_grid(
                        fake[:32], normalize=True
                    )

                    writer.add_image(
                        'real', img_grid_real, global_step=tb_update_idx)
                    writer.add_image(
                        "fake", img_grid_fake, global_step=tb_update_idx)

                tb_update_idx += 1

        if config.SAVE_MODEL:
            save(generator, optim_g, 'generator.pth')
            save(discriminator, optim_d, 'discriminator.pth')
