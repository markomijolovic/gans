import torch
import torch.nn as nn
import torch.optim as optim


def save(model: nn.Module, optimizer: optim.Optimizer, filename: str) -> None:
    print(f'saving checkpoint to {filename}')

    checkpoint = {'state_dict': model.state_dict(
    ), 'optimizer': optimizer.state_dict(), }
    torch.save(checkpoint, filename)


def load(model: nn.Module, optimizer: optim.Optimizer, lr: float, filename: str) -> None:
    print(f'loading checkpoint from file {filename}')

    checkpoint = torch.load(filename, map_location='cuda')
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
