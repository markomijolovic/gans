import torch

torch.backends.cudnn.benchmark = True

STARTING_SIZE = 4
DATASET_PATH = '/home/marko/Downloads/celeba_hq/train'
DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
LOAD_MODEL = False
SAVE_MODEL = True
LEARNING_RATE = 1e-3
BATCH_SIZES = [256, 128, 64, 32, 16, 4, 2]
CHANNELS_IMG = 3
Z_DIM = 256
IN_CHANNELS = 256
TB_UPDATE_STEP = 10
LAMBDA = 10
PROGRESSIVE_EPOCHS = [5] * len(BATCH_SIZES)
NUM_WORKERS = 8
FIXED_NOISE = torch.randn(8, Z_DIM, 1, 1).to(DEVICE)
