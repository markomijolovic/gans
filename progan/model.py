from typing import Any

import torch
import torch.nn as nn
import torch.nn.functional as F

factors = [1, 1, 1, 1, 1 / 2, 1 / 4, 1 / 8, 1 / 16, 1 / 32]


class EqualizedConv2d(nn.Conv2d):
    def __init__(self, in_channels: int, out_channels: int, kernel_size: int = 3, stride: int = 1,
                 padding: int = 1) -> None:
        super().__init__(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride,
                         padding=padding)
        self.scale = (2 / (in_channels * (kernel_size ** 2))) ** 0.5

        nn.init.normal_(self.weight)
        nn.init.zeros_(self.bias)

    def forward(self, x):
        return torch.conv2d(input=x, weight=self.weight * self.scale, bias=self.bias, stride=self.stride,
                            padding=self.padding)


class EqualizedConvTranspose2d(nn.ConvTranspose2d):
    def __init__(self, in_channels: int, out_channels: int, kernel_size: int = 3, stride: int = 1,
                 padding: int = 1, ) -> None:
        super().__init__(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride,
                         padding=padding)
        torch.nn.init.normal_(self.weight)
        torch.nn.init.zeros_(self.bias)

        self.scale = (2 / (in_channels * (kernel_size ** 2))) ** 0.5

    def forward(self, x: torch.Tensor, output_size: Any = None) -> torch.Tensor:
        output_padding = self._output_padding(
            x, output_size, self.stride, self.padding, self.kernel_size)
        return torch.conv_transpose2d(input=x, weight=self.weight * self.scale, bias=self.bias, stride=self.stride,
                                      padding=self.padding, output_padding=output_padding, groups=self.groups, dilation=self.dilation, )


class PixelNorm(nn.Module):
    def __init__(self) -> None:
        super().__init__()

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return x / torch.sqrt(torch.mean(x ** 2, dim=1, keepdim=True) + 1e-8)


class ConvBlock(nn.Module):
    def __init__(self, in_channels: int, out_channels: int, use_pixelnorm: bool = True) -> None:
        super().__init__()

        self.use_pn = use_pixelnorm
        self.pn = PixelNorm()

        self.conv1 = EqualizedConv2d(in_channels, out_channels)
        self.conv2 = EqualizedConv2d(out_channels, out_channels)

        self.leaky = nn.LeakyReLU(0.2)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.leaky(self.conv1(x))
        x = self.pn(x) if self.use_pn else x
        x = self.leaky(self.conv2(x))
        x = self.pn(x) if self.use_pn else x
        return x


def minibatch_std(x: torch.Tensor) -> torch.Tensor:
    batch_statistics = (torch.std(x, dim=0).mean().repeat(
        x.shape[0], 1, x.shape[2], x.shape[3]))
    return torch.cat([x, batch_statistics], dim=1)


class Generator(nn.Module):
    def __init__(self, z_dim: int, in_channels: int, img_channels: int = 3) -> None:
        super().__init__()

        self.initial = nn.Sequential(PixelNorm(),
                                     EqualizedConvTranspose2d(
                                         in_channels=z_dim, out_channels=in_channels, kernel_size=4, stride=1, padding=0),
                                     nn.LeakyReLU(0.2), PixelNorm(),
                                     EqualizedConv2d(in_channels=in_channels, out_channels=in_channels,
                                                     kernel_size=3, stride=1, padding=1),
                                     nn.LeakyReLU(0.2), PixelNorm(), )

        self.initial_rgb = EqualizedConv2d(
            in_channels, img_channels, kernel_size=1, stride=1, padding=0)
        self.blocks, self.rgb_blocks = (
            nn.ModuleList([]), nn.ModuleList([self.initial_rgb]),)

        for i in range(len(factors) - 1):
            conv_block_in = int(in_channels * factors[i])
            conv_block_out = int(in_channels * factors[i + 1])
            self.blocks.append(ConvBlock(conv_block_in, conv_block_out))
            self.rgb_blocks.append(EqualizedConv2d(
                conv_block_out, img_channels, kernel_size=1, stride=1, padding=0))

    def fade_in(self, alpha: float, upscaled: torch.Tensor, generated: torch.Tensor) -> torch.Tensor:
        return torch.tanh(alpha * generated + (1 - alpha) * upscaled)

    def forward(self, x: torch.Tensor, alpha: float, steps: int) -> torch.Tensor:
        y = self.initial(x)

        if steps == 0:
            return self.initial_rgb(y)

        for step in range(steps):
            upscaled = F.interpolate(y, scale_factor=2, mode='nearest')
            y = self.blocks[step](upscaled)

        upscaled = self.rgb_blocks[steps - 1](upscaled)
        y = self.rgb_blocks[steps](y)
        return self.fade_in(alpha, upscaled, y)


class Discriminator(nn.Module):
    def __init__(self, in_channels: int, img_channels: int = 3) -> None:
        super().__init__()
        self.blocks, self.rgb_blocks = nn.ModuleList(
            []), nn.ModuleList([])
        self.leaky = nn.LeakyReLU(0.2)

        for i in range(len(factors) - 1, 0, -1):
            conv_block_in = int(in_channels * factors[i])
            conv_block_out = int(in_channels * factors[i - 1])
            self.blocks.append(
                ConvBlock(conv_block_in, conv_block_out, use_pixelnorm=False))
            self.rgb_blocks.append(EqualizedConv2d(
                img_channels, conv_block_in, kernel_size=1, stride=1, padding=0))

        self.initial_rgb = EqualizedConv2d(
            img_channels, in_channels, kernel_size=1, stride=1, padding=0)
        self.rgb_blocks.append(self.initial_rgb)
        self.avg_pool = nn.AvgPool2d(kernel_size=2, stride=2)

        self.final_block = nn.Sequential(EqualizedConv2d(in_channels + 1, in_channels, kernel_size=3, padding=1),
                                         nn.LeakyReLU(0.2), EqualizedConv2d(
                                             in_channels, in_channels, kernel_size=4, padding=0, stride=1),
                                         nn.LeakyReLU(0.2), EqualizedConv2d(in_channels, 1, kernel_size=1, padding=0, stride=1), )

    def fade_in(self, alpha: float, downscaled: torch.Tensor, out: torch.Tensor) -> torch.Tensor:
        return alpha * out + (1 - alpha) * downscaled

    def forward(self, x: torch.Tensor, alpha: float, steps: int) -> torch.Tensor:
        current_step = len(self.blocks) - steps

        y = self.leaky(self.rgb_blocks[current_step](x))

        if steps == 0:
            y = minibatch_std(y)
            return self.final_block(y).view(y.shape[0], -1)

        downscaled = self.leaky(
            self.rgb_blocks[current_step + 1](self.avg_pool(x)))
        y = self.avg_pool(self.blocks[current_step](y))
        y = self.fade_in(alpha, downscaled, y)

        for step in range(current_step + 1, len(self.blocks)):
            y = self.blocks[step](y)
            y = self.avg_pool(y)

        y = minibatch_std(y)
        return self.final_block(y).view(y.shape[0], -1)
