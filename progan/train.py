from math import log2

import torch
import torch.optim as optim
import torchvision
import torchvision.utils
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

import config
from model import Discriminator, Generator
from utils import (generate_examples, gradient_penalty,
                   save, load, get_loader)


def main() -> None:
    gen = Generator(config.Z_DIM, config.IN_CHANNELS, img_channels=config.CHANNELS_IMG).to(
        config.DEVICE)
    disc = Discriminator(
        config.IN_CHANNELS, config.CHANNELS_IMG).to(config.DEVICE)

    optim_g = optim.Adam(
        gen.parameters(), lr=config.LEARNING_RATE, betas=(0.0, 0.99))
    optim_d = optim.Adam(disc.parameters(),
                         lr=config.LEARNING_RATE, betas=(0.0, 0.99))

    writer = SummaryWriter('logs/progan')
    if config.LOAD_MODEL:
        load(gen, optim_g, config.LEARNING_RATE, 'generator.pth')
        load(disc, optim_d,
             config.LEARNING_RATE, 'discriminator.pth')

    tensorboard_step = 0
    step = int(log2(config.STARTING_SIZE / 4))
    for num_epochs in config.PROGRESSIVE_EPOCHS[step:]:
        alpha = 0.0
        loader, dataset = get_loader(4 * 2 ** step)
        print(f'current image size: {4 * 2 ** step}')

        for epoch in range(num_epochs):
            print(f'epoch: {epoch + 1}/{num_epochs}')

            iterator = tqdm(loader, leave=True)
            for batch_idx, (real, _) in enumerate(iterator):
                real = real.to(config.DEVICE)
                batch_size = real.shape[0]

                noise = torch.randn(batch_size, config.Z_DIM,
                                    1, 1).to(config.DEVICE)

                fake = gen(noise, alpha, step)
                critic_real = disc(real, alpha, step)
                critic_fake = disc(fake.detach(), alpha, step)
                gp = gradient_penalty(disc, real, fake,
                                      alpha, step, device=config.DEVICE)
                loss_discriminator = (-(torch.mean(critic_real) - torch.mean(critic_fake)) + config.LAMBDA * gp + (
                    0.001 * torch.mean(critic_real ** 2)))

                optim_d.zero_grad()
                loss_discriminator.backward()
                optim_d.step()

                gen_fake = disc(fake, alpha, step)
                loss_gen = -torch.mean(gen_fake)

                optim_g.zero_grad()
                loss_gen.backward()
                optim_g.step()

                alpha += batch_size / \
                    ((config.PROGRESSIVE_EPOCHS[step] * 0.5) * len(dataset))
                alpha = min(alpha, 1)

                if batch_idx % config.TB_UPDATE_STEP == 0:
                    with torch.no_grad():
                        fixed_fakes = gen(config.FIXED_NOISE,
                                          alpha, step) * 0.5 + 0.5

                    writer.add_scalar(
                        'loss discriminator', loss_discriminator.item(), global_step=tensorboard_step)

                    with torch.no_grad():
                        real_grid = torchvision.utils.make_grid(
                            real.detach()[:8], normalize=True)
                        fake_grid = torchvision.utils.make_grid(
                            fixed_fakes.detach()[:8], normalize=True)
                        writer.add_image('real', real_grid,
                                         global_step=tensorboard_step)
                        writer.add_image('fake', fake_grid,
                                         global_step=tensorboard_step)
                    tensorboard_step += 1
                iterator.set_postfix(
                    gp=gp.item(), loss_critic=loss_discriminator.item(), )

            if config.SAVE_MODEL:
                save(gen, optim_g, filename='generator.pth')
                save(disc, optim_d, filename='discriminator.pth')

        step += 1


if __name__ == '__main__':
    main()
