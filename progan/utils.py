from pathlib import Path
from typing import Tuple

from math import log2

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data.dataset import Dataset
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torchvision.utils import save_image

import config
from model import Discriminator
from model import Generator


def get_loader(image_size: int) -> Tuple[DataLoader, Dataset]:
    transform = transforms.Compose([transforms.Resize((image_size, image_size)),
                                    transforms.ToTensor(),
                                    transforms.Normalize([0.5 for _ in range(config.CHANNELS_IMG)], [0.5 for _ in range(config.CHANNELS_IMG)], ), ])
    batch_size = config.BATCH_SIZES[int(log2(image_size / 4))]
    dataset = datasets.ImageFolder(
        root=config.DATASET_PATH, transform=transform)
    loader = DataLoader(dataset, batch_size=batch_size,
                        shuffle=True, num_workers=config.NUM_WORKERS)
    return loader, dataset


def gradient_penalty(discriminator: Discriminator, real: torch.Tensor, fake: torch.Tensor, alpha: float, train_step: int,
                     device: str) -> torch.Tensor:
    batch_size, channels, height, width = real.shape
    beta = torch.rand((batch_size, 1, 1, 1)).repeat(
        1, channels, height, width).to(device)
    interpolated = real * beta + fake.detach() * (1 - beta)
    interpolated.requires_grad_()

    mixed_scores = discriminator(interpolated, alpha, train_step)
    gradient = torch.autograd.grad(inputs=interpolated, outputs=mixed_scores, grad_outputs=torch.ones_like(mixed_scores),
                                   create_graph=True, retain_graph=True)[0]
    gradient = gradient.view(gradient.shape[0], -1)
    gradient_norm = gradient.norm(2, dim=1)
    return torch.mean((gradient_norm - 1) ** 2)


def save(model: nn.Module, optimizer: optim.Optimizer, filename: str) -> None:
    print(f'saving checkpoint to {filename}')

    checkpoint = {'state_dict': model.state_dict(
    ), 'optimizer': optimizer.state_dict(), }
    torch.save(checkpoint, filename)


def load(model: nn.Module, optimizer: optim.Optimizer, lr: float, filename: str) -> None:
    print(f'loading checkpoint from file {filename}')

    checkpoint = torch.load(filename, map_location='cuda')
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def generate_examples(gen: Generator, steps: int, n: int = 100) -> None:
    Path('generated_images').mkdir(parents=True, exist_ok=True)

    gen.eval()
    for i in range(n):
        with torch.no_grad():
            noise = torch.randn((1, config.Z_DIM, 1, 1))
            img = gen(noise, 1.0, steps)*0.5+0.5
            save_image(img, f'generated_images/img_{i}.png')
    gen.train()
